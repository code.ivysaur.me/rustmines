# rustmines

![](https://img.shields.io/badge/written%20in-Rust-blue)

An ncurses minesweeper clone

Tags: game

## Features

- Coloured squares
- Keyboard or mouse control

## Usage


```
Usage:
  rustmines [options]

Options:
  --help            Display this message
  --width NUM       Board width (default 9)
  --height NUM      Board height (default 9)
  --mines NUM       Number of mines (default 10)
  --easy            9x9 board with 10 mines
  --intermediate    16x16 board with 40 mines
  --advanced        30x16 board with 99 mines

Controls:
  [arrow keys]      Move cursor
  w a s d           Move cursor
  f                 Flag square
  [space]           Reveal square
  [left-click]      Move cursor and reveal square
  [right-click]     Move cursor and flag square
  [double-click]    Move cursor, reveal square + unflagged neighbours
```



## Download

- [⬇️ rustmines-1.0.0-win64.7z](dist-archive/rustmines-1.0.0-win64.7z) *(69.44 KiB)*
- [⬇️ rustmines-1.0.0-src.7z](dist-archive/rustmines-1.0.0-src.7z) *(4.63 KiB)*
